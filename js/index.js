const pageElement = document.getElementById('page');
const speedElement = document.getElementById('speed');
const statusElement = document.getElementById('status');
const unitsElement = document.getElementById('units');
const maxSpeedElement = document.getElementById('max-speed');
const themeSelectElement = document.getElementById('theme');

let units = 'mph';
let maxSpeed = -1;
let theme = 'auto';

function updateSpeed(speed = '') {
    speedElement.innerHTML = speed;
}

function updateStatus(status = '') {
    statusElement.innerHTML = status;
}

function updateMaxSpeed(speed) {
    maxSpeed = speed > maxSpeed ? speed : maxSpeed;
    maxSpeedElement.textContent = `${maxSpeed}`;
}

function updateUnits() {
    unitsElement.textContent = units;
}

function updateThemeClass(newTheme = 'night') {
    theme = newTheme;
    if (newTheme === 'night') {
        pageElement.classList.add('night');
        pageElement.classList.remove('day');
    }
    if (newTheme === 'day') {
        pageElement.classList.add('day');
        pageElement.classList.remove('night');
    }
    if (newTheme === 'auto') {
        // window.ondevicelight.
    }
}

function updateTheme(event) {
    updateThemeClass(event.srcElement.value);
}


function convertSpeed(metersPerSecond, unit = 'mph') {
    const ratio = unit === 'mph' ? 2.23694 : 3.6
    return metersPerSecond * ratio;
}

const requestWakeLock = async () => {
    try {
        const wakeLock = await navigator.wakeLock.request('screen');
    } catch (err) {
        // the wake lock request fails - usually system related, such low as battery
        console.log(`${err.name}, ${err.message}`);
        updateStatus('<span style="text-decoration: line-through;">Wakelock</span>');
    }
}

function init() {
    requestWakeLock();
    updateStatus('Loading');
    updateUnits();

    themeSelectElement.addEventListener('change', updateTheme);
    updateThemeClass('night');

    if('geolocation' in navigator) {
        // geolocation is available so update status
        updateStatus('geo');

        // request full screen
        if (!document.fullscreenElement) {
            pageElement.requestFullscreen().catch(err => {
                console.log(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
                updateStatus('Unable to maximize');
            });
        } else {
            document.exitFullscreen();
        }

        // add a watch for the location aka gps
        const watchID = navigator.geolocation.watchPosition(
            (position) => {
                let speedMPS = position.coords.speed;
                if (speedMPS !== null) {
                    let speed = Math.round(convertSpeed(speedMPS, units));
                    updateStatus('ok');
                    updateSpeed(speed);
                    updateMaxSpeed(speed);
                } else {
                    updateStatus('Speed Unavailable');
                    updateSpeed('err');
                }
            },
            () => {
                updateStatus('Speed Unavailable');
                updateSpeed('err');
            },
            {
                enableHighAccuracy: true,
                maximumAge: 1000,
                timeout: 990
            }
        );
    } else {
        /* geolocation IS NOT available */
        updateStatus('<span style="text-decoration: line-through;">Geolocation</span>');
    }

    // update theme based on amount of light
    // if ('ondevicelight' in window) {
    //     window.addEventListener('devicelight', function(event) {
    //         if (theme === 'auto') {
    //             if (event.value < 50) {
    //                 pageElement.classList.add('night');
    //                 pageElement.classList.remove('day');
    //             } else {
    //                 pageElement.classList.add('day');
    //                 pageElement.classList.remove('night');
    //             }
    //         }
    //     });
    // } else {
    //     console.log('devicelight event not supported');
    // }
}

init();